#!/usr/bin/python3
#
#       main_app_4
#
from sys import argv
from years_values_counter import years_values_counter as counter
from years_values_report  import years_values_report as report
# 
def main() :

    carss = {}

    with open(argv[1],"r") as fd:
        data = fd.readlines()

    counter(data,carss)

    report(carss)

if __name__ == "__main__" : main()
