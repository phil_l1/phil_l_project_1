#
#       years_values_counter 
#
def years_values_counter(data,dictionary) :
    total = 0
    for line in data : 
        array = line.split()

        total += int(array[4])
            
        if array[2] in dictionary :
            dictionary[array[2]].append(array[0] + " $ " + str(array[4]))
        else : 
            dictionary[array[2]] = [array[0] + " $ " + str(array[4])]
    
    dictionary['total'] = total
