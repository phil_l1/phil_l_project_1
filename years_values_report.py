#
#       years_values_report 
#
def years_values_report(dictionary) : 

    for key,data in iter(dictionary.items()) :
        if key == 'total' :
            total = data
            break
        row = " ".join(dictionary[key])
        print('%-3s=%3s  %s' % (key,len(data),row))
    print("=" * 50)
    print("Total inventory value: $",total)
